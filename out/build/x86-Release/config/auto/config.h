#ifndef AUTO_CONFIG_H
#define AUTO_CONFIG_H

/* #undef DEBUG */

#define SIZEOF_INT 4
#define SIZEOF_LONG 4

#if 4 == 8
#define ARCH_64
#elif 4 == 4
#define ARCH_32
#endif

#define PROJECT_NAME "nvim"

/* #undef HAVE__NSGETENVIRON */
/* #undef HAVE_FD_CLOEXEC */
/* #undef HAVE_FSEEKO */
/* #undef HAVE_GETPWENT */
/* #undef HAVE_GETPWNAM */
/* #undef HAVE_GETPWUID */
#define HAVE_ICONV
/* #undef HAVE_LANGINFO_H */
#define HAVE_LOCALE_H
/* #undef HAVE_NL_LANGINFO_CODESET */
/* #undef HAVE_NL_MSG_CAT_CNTR */
/* #undef HAVE_PWD_H */
/* #undef HAVE_READLINK */
/* #undef HAVE_SETPGID */
/* #undef HAVE_SETSID */
/* #undef HAVE_SIGACTION */
/* #undef HAVE_STRCASECMP */
/* #undef HAVE_STRINGS_H */
/* #undef HAVE_STRNCASECMP */
/* #undef HAVE_SYS_UTSNAME_H */
/* #undef HAVE_SYS_WAIT_H */
/* #undef HAVE_TERMIOS_H */
#define HAVE_WORKING_LIBINTL
/* #undef HAVE_WSL */
/* #undef UNIX */
#define USE_FNAME_CASE
/* #undef HAVE_SYS_UIO_H */
#ifdef HAVE_SYS_UIO_H
/* #undef HAVE_READV */
# ifndef HAVE_READV
#  undef HAVE_SYS_UIO_H
# endif
#endif

#define FEAT_TUI

#ifndef UNIT_TESTING
/* #undef LOG_LIST_ACTIONS */
#endif

/* #undef HAVE_BE64TOH */
/* #undef ORDER_BIG_ENDIAN */
#define ENDIAN_INCLUDE_FILE <endian.h>

/* #undef HAVE_EXECINFO_BACKTRACE */
/* #undef HAVE_BUILTIN_ADD_OVERFLOW */

#endif  // AUTO_CONFIG_H
