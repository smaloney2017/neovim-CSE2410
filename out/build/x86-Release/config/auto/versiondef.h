#ifndef AUTO_VERSIONDEF_H
#define AUTO_VERSIONDEF_H

#define NVIM_VERSION_MAJOR 0
#define NVIM_VERSION_MINOR 5
#define NVIM_VERSION_PATCH 0
#define NVIM_VERSION_PRERELEASE "-dev"

/* #undef NVIM_VERSION_MEDIUM */
#ifndef NVIM_VERSION_MEDIUM
# include "auto/versiondef_git.h"
#endif

#define NVIM_API_LEVEL 7
#define NVIM_API_LEVEL_COMPAT 0
#define NVIM_API_PRERELEASE true

#define NVIM_VERSION_CFLAGS "C:/Program Files (x86)/Microsoft Visual Studio/2019/Community/VC/Tools/MSVC/14.24.28314/bin/HostX86/x86/cl.exe /DWIN32 /D_WINDOWS /W3 /MD /Zi /O2 /Ob1 /DNDEBUG /W3 -D_CRT_SECURE_NO_WARNINGS -D_CRT_NONSTDC_NO_DEPRECATE -DWIN32 -D_WIN32_WINNT=0x0600 -DINCLUDE_GENERATED_DECLARATIONS -DUTF8PROC_STATIC -DNVIM_MSGPACK_HAS_FLOAT32 -DNVIM_UNIBI_HAS_VAR_FROM -DMIN_LOG_LEVEL=3 -IE:/neovim-CSE2410/out/build/x86-Release/config -IE:/neovim-CSE2410/src -IE:/neovim-CSE2410/out/build/x86-Release/.deps/usr/include -IE:/neovim-CSE2410/out/build/x86-Release/src/nvim/auto -IE:/neovim-CSE2410/out/build/x86-Release/include"
#define NVIM_VERSION_BUILD_TYPE "RelWithDebInfo"

#endif  // AUTO_VERSIONDEF_H
