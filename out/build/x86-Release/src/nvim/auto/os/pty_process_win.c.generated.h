#define DEFINE_FUNC_ATTRIBUTES
#include "nvim/func_attr.h"
#undef DEFINE_FUNC_ATTRIBUTES
static void __stdcall pty_process_finish1(void *context, BOOLEAN unused) FUNC_ATTR_NONNULL_ALL;
static void pty_process_connect_cb(uv_connect_t *req, int status) FUNC_ATTR_NONNULL_ALL;
static void wait_eof_timer_cb(uv_timer_t *wait_eof_timer) FUNC_ATTR_NONNULL_ALL;
static void pty_process_finish2(PtyProcess *ptyproc) FUNC_ATTR_NONNULL_ALL;
static int build_cmd_line(char **argv, wchar_t **cmd_line, _Bool is_cmdexe) FUNC_ATTR_NONNULL_ALL;
static void quote_cmd_arg(char *dest, size_t dest_remaining, const char *src) FUNC_ATTR_NONNULL_ALL;
#include "nvim/func_attr.h"
