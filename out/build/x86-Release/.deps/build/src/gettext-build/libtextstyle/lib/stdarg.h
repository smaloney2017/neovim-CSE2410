/* Substitute for and wrapper around <stdarg.h>.
   Copyright (C) 2008-2019 Free Software Foundation, Inc.

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, see <https://www.gnu.org/licenses/>.  */

#ifndef _0_STDARG_H

#if __GNUC__ >= 3
0
#endif


/* The include_next requires a split double-inclusion guard.  */
#include <../include/stdarg.h>

#ifndef _0_STDARG_H
#define _0_STDARG_H

#ifndef va_copy
# define va_copy(a,b) ((a) = (b))
#endif

#endif /* _0_STDARG_H */
#endif /* _0_STDARG_H */
