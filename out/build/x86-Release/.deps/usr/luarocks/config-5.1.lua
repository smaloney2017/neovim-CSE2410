rocks_trees = {
    home..[[/luarocks]],
    { name = [[user]],
         root    = home..[[/luarocks]],
    },
    { name = [[system]],
         root    = [[E:/neovim-CSE2410/out/build/x86-Release/.deps/usr]],
         bin_dir = [[E:/neovim-CSE2410/out/build/x86-Release/.deps/usr/bin]],
         lib_dir = [[E:/neovim-CSE2410/out/build/x86-Release/.deps/usr/bin]],
         lua_dir = [[E:/neovim-CSE2410/out/build/x86-Release/.deps/usr/bin/lua]],
    },
}
variables = {
    MSVCRT = 'VCRUNTIME140',
    LUALIB = 'lua51.lib',
}
verbose = false   -- set to 'true' to enable verbose output
