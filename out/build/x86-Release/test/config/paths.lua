local module = {}

module.include_paths = {}
for p in ("E:/neovim-CSE2410/out/build/x86-Release/config;E:/neovim-CSE2410/src;E:/neovim-CSE2410/out/build/x86-Release/.deps/usr/include;E:/neovim-CSE2410/out/build/x86-Release/.deps/usr/include;E:/neovim-CSE2410/out/build/x86-Release/.deps/usr/include;E:/neovim-CSE2410/out/build/x86-Release/.deps/usr/include;E:/neovim-CSE2410/out/build/x86-Release/.deps/usr/include;E:/neovim-CSE2410/out/build/x86-Release/.deps/usr/include;E:/neovim-CSE2410/out/build/x86-Release/.deps/usr/include;E:/neovim-CSE2410/out/build/x86-Release/.deps/usr/include;E:/neovim-CSE2410/out/build/x86-Release/.deps/usr/include;E:/neovim-CSE2410/out/build/x86-Release/.deps/usr/include" .. ";"):gmatch("[^;]+") do
  table.insert(module.include_paths, p)
end

module.test_build_dir = "E:/neovim-CSE2410/out/build/x86-Release"
module.test_include_path = module.test_build_dir .. "/test/includes/post"
module.test_libnvim_path = "E:/neovim-CSE2410/out/build/x86-Release/lib/nvim-test.dll"
module.test_source_path = "E:/neovim-CSE2410"
module.test_lua_prg = "E:/neovim-CSE2410/out/build/x86-Release/.deps/usr/bin/luajit.exe"
module.test_luajit_prg = ""
if module.test_luajit_prg == '' then
  if module.test_lua_prg:sub(-6) == 'luajit' then
    module.test_luajit_prg = module.test_lua_prg
  else
    module.test_luajit_prg = nil
  end
end
table.insert(module.include_paths, "E:/neovim-CSE2410/out/build/x86-Release/include")

return module
