#define DEFINE_FUNC_ATTRIBUTES
#include "nvim/func_attr.h"
#undef DEFINE_FUNC_ATTRIBUTES
_Bool os_has_conpty_working(void);
TriState os_dyn_conpty_init(void);
conpty_t *os_conpty_init(char **in_name, char **out_name, uint16_t width, uint16_t height);
_Bool os_conpty_spawn(conpty_t *conpty_object, HANDLE *process_handle, wchar_t *name, wchar_t *cmd_line, wchar_t *cwd, wchar_t *env);
void os_conpty_set_size(conpty_t *conpty_object, uint16_t width, uint16_t height);
void os_conpty_free(conpty_t *conpty_object);
#include "nvim/func_attr.h"
