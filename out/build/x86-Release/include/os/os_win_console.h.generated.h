#define DEFINE_FUNC_ATTRIBUTES
#include "nvim/func_attr.h"
#undef DEFINE_FUNC_ATTRIBUTES
int os_get_conin_fd(void);
void os_replace_stdin_to_conin(void);
void os_replace_stdout_and_stderr_to_conout(void);
#include "nvim/func_attr.h"
